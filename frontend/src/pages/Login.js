import React, { Component } from 'react'
import axios from 'axios'
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap'
import { BrowserRouter as Router } from 'react-router-dom'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      formData: {
        username: '',
        password: ''
      }
    }
  }

  onChange = e => {
    let { name, value } = e.target
    const formData = { ...this.state.formData, [name]: value }
    this.setState({ formData })

    console.log(this.state.formData)
  }
  onLogin = () => {
    axios
      .get(`api/user/`, {
        params: {
          username: this.state.formData.username,
          password: this.state.formData.password
        }
      })
      .then(res =>
        res.data === 'none'
          ? null
          : (localStorage.setItem('username', res.data.username),
            localStorage.setItem('group', res.data.group),
            (window.location.href = '/'))
      )
  }

  render () {
    return (
      <Router>
        <Container className='themed-container' fluid='md'>
          <Form>
            <FormGroup>
              <Label for='exampleUsername'>ชื่อผู้ใช้</Label>
              <Input
                type='text'
                name='username'
                id='exampleUsername'
                placeholder='username placeholder'
                onChange={e => this.onChange(e)}
                required
              />
              <Label for='examplePassword'>รหัสผ่าน</Label>
              <Input
                type='password'
                name='password'
                id='examplePassword'
                placeholder='password placeholder'
                onChange={e => this.onChange(e)}
                required
              />
              <br></br>
              <Button color='success' onClick={() => this.onLogin()}>
                ล็อกอิน
              </Button>
            </FormGroup>
          </Form>
        </Container>
      </Router>
    )
  }
}

export default App
