from django.shortcuts import render
from rest_framework import viewsets
from .serializers import ScheduleSerializer,UserSerializer,HNUserSerializer,DepartmentSerializer,LeaveFormSerializer
from .models import Schedule,HNUser,Department,LeaveForm
from django.core.serializers import serialize
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate
from itertools import chain
# Create your views here.

class ScheduleView(viewsets.ModelViewSet):
    serializer_class = ScheduleSerializer
    queryset = Schedule.objects.all().order_by('month')

class HNUserView(viewsets.ModelViewSet):
    serializer_class = HNUserSerializer
    queryset = HNUser.objects.all().order_by('hn')

class DepartmentView(viewsets.ModelViewSet):
    serializer_class = DepartmentSerializer
    queryset = Department.objects.all().order_by('Off_id')

class LeaveFormView(viewsets.ModelViewSet):
    serializer_class = LeaveFormSerializer
    queryset = LeaveForm.objects.all().order_by('status')

@api_view(['GET'])
def login(request):
    username = request.GET.get('username')
    password = request.GET.get('password')    
    queryset = authenticate(username=username, password=password)
    if(not queryset == None):
        queryset.group = queryset.groups.get()
        serializer_class = UserSerializer(queryset,many=False)
        
        return Response(serializer_class.data)
    
    return Response('none')


@api_view(['GET'])
def get_some_schedule(request,username):
    try:
        queryset = Schedule.objects.filter(name=username)
        serializer_class = ScheduleSerializer(queryset,many=True)
        return Response(serializer_class.data)
    except:
        return Response([])

@api_view(('GET',))
def get_department(request):        
    find = request.GET.get('search')  
    try:
        queryset = Department.objects.get(Off_id=find)
        serializer_class = DepartmentSerializer(queryset,many=False)
        return Response(serializer_class.data)
    except :
        return Response([])
    
@api_view(('GET',))
def get_hn(request,hn_id):        
    try:   
        queryset = HNUser.objects.get(hn=hn_id)
        serializer_class = HNUserSerializer(queryset,many=False)
        return Response(serializer_class.data)
    except :
        return Response({})