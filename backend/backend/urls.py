from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from schedule import views

router = routers.DefaultRouter()
router.register(r'schedules', views.ScheduleView, 'schedule')
router.register(r'hnusers', views.HNUserView, 'hnuser')
router.register(r'departments', views.DepartmentView, 'departmet')
router.register(r'leaveforms', views.LeaveFormView, 'leaveform')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)), # get all of schedule
    path('api/schedule/',views.get_some_schedule),
    path('api/user/',views.login),
    path('api/hn/<hn_id>/',views.get_hn),
    path('api/department/',views.get_department),     
]
