import { Component } from 'react'
import {
  FormGroup,
  Label,
  Input,
  Container,
  Button,
  Col,
  Row,
  Table
} from 'reactstrap'
import axios from 'axios'
import 'react-date-range/dist/styles.css' // main style file
import 'react-date-range/dist/theme/default.css' // theme css file
import LeavePage from '../components/Leave'
import SickLeavePage from '../components/SickLeave'
import { DateRange } from 'react-date-range'

class App extends Component {
  constructor (props) {
    super(props)
    axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN'
    axios.defaults.xsrfCookieName = 'csrftoken'
    this.state = {
      selectionDepartment: [],
      selectedDepartment: [],
      hn: {},
      selectionRange: {
        startDate: new Date(),
        endDate: new Date(),
        key: 'selection'
      },
      reason: {},
      formType: [],
      countOfLeave: 0
    }
  }

  componentDidMount () {
    axios
      .get('api/departments/')
      .then(res =>
        this.setState({ selectionDepartment: res.data }, () =>
          this.setState({ selectedDepartment: res.data[0].Off_id })
        )
      )
  }

  handleSelect = ranges => {
    this.setState({ selectionRange: ranges.selection })
  }

  dateFormat = date => {
    let day = date.getDate()
    let month = date.getMonth()
    let year = date.getFullYear()
    return month + 1 < 10
      ? `${year}-0${month + 1}-${day}`
      : `${year}-${month + 1}-${day}`
  }

  handleSubmit = () => {
    let payload = {
      Off_id: this.state.selectedDepartment,
      hn: this.state.hn,
      start_date: this.dateFormat(this.state.selectionRange.startDate),
      end_date: this.dateFormat(this.state.selectionRange.endDate),
      reason: this.state.reason
    }

    axios.post('api/leaveforms/', payload)
  }

  handdleChange = e => {
    if (e.target.name === 'select') {
      console.log(e.target.value)
      return this.setState({ selectedDepartment: e.target.value })
    } else if (e.target.name === 'hn') {
      return this.setState({ hn: e.target.value })
    } else if (e.target.name === 'formType') {
      return this.setState({ formType: e.target.value })
    }
    return this.setState({ reason: e.target.value })
  }

  render () {
    return (
      <Container>
        <FormGroup>
          <Label for='exampleSelect'>ประเภทการลา</Label>
          <Input
            type='select'
            name='formType'
            id='exampleSelect'
            onChange={e => this.handdleChange(e)}
          >
            {/* {this.state.formType.map(item => (
              <option key={item}>{item}</option>
            ))} */}
            <option>ใบลาป่วย ลาคลอด ลากิจส่วนตัว</option>
            <option>ใบลาพักผ่อน</option>
          </Input>
        </FormGroup>
        <Row>
          <Col xs='6'>
            <FormGroup>
              <Label for='exampleEmail'>HN</Label>
              <Input
                type='text'
                name='hn'
                id='exampleEmail'
                placeholder='HN'
                onChange={e => this.handdleChange(e)}
              />
            </FormGroup>
          </Col>
          <Col xs='3'>
            <FormGroup>
              <Label for='exampleSelect'>แผนก</Label>
              <Input
                type='select'
                name='select'
                id='exampleSelect'
                onChange={e => this.handdleSelect(e)}
              >
                {this.state.selectionDepartment.map(item => (
                  <option key={item.Off_id} value={item.Off_id}>
                    {item.Off_name}
                  </option>
                ))}
              </Input>
            </FormGroup>
          </Col>
          <Col sm='3'>
            <FormGroup>
              <Label for='exampleEmail'>เป็น</Label>
              <Input
                type='select'
                name='position'
                id='exampleSelect'
                // onChange={e => this.handdleChange(e)}
              >
                {/* {this.state.formType.map(item => (
              <option key={item}>{item}</option>
            ))} */}
                <option>ข้าราชการ</option>
                <option>พนักงานราชการ</option>
                <option>ลูกจ้างประจำ</option>
                <option>พนักงานกระทรวงสาธารณสุข</option>
                <option>ลูกจ้างชั่วคราว</option>
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col sm='4'>
            <FormGroup>
              <Label>ตำแหน่ง</Label>
              <Input
                type='text'
                name='text'
                id='exampleText'
                onChange={e => this.handdleChange(e)}
              />
            </FormGroup>
          </Col>
          <Col sm='4'>
            <FormGroup>
              <Label>สังกัดจุด/ฝ่าย</Label>
              <Input
                type='text'
                name='text'
                id='exampleText'
                onChange={e => this.handdleChange(e)}
              />
            </FormGroup>
          </Col>
          <Col sm='4'>
            <FormGroup>
              <Label>กลุ่มงาน</Label>
              <Input
                type='text'
                name='text'
                id='exampleText'
                onChange={e => this.handdleChange(e)}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col sm='2'>
            <FormGroup>
              <Label for='exampleSelect'>ประสงค์ขอลา</Label>
              <FormGroup check>
                <Label check>
                  <Input
                    type='radio'
                    name='radio'
                    disabled={this.state.formType === 'ใบลาพักผ่อน'}
                  />
                  ป่วย
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check>
                  <Input
                    type='radio'
                    name='radio'
                    disabled={this.state.formType === 'ใบลาพักผ่อน'}
                  />
                  กิจส่วนตัว
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check>
                  <Input
                    type='radio'
                    name='radio'
                    disabled={this.state.formType === 'ใบลาพักผ่อน'}
                  />
                  ลาคลอดบุตร
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check>
                  <Input
                    type='radio'
                    name='radio'
                    checked={this.state.formType === 'ใบลาพักผ่อน'}
                    disabled={this.state.formType !== 'ใบลาพักผ่อน'}
                  />
                  ลาพักผ่อน
                </Label>
              </FormGroup>
            </FormGroup>
          </Col>
          <Col sm='10'>
            <FormGroup>
              <Label for='exampleText'>เนื่องจาก</Label>
              <Input
                type='textarea'
                name='text'
                id='exampleText'
                style={{ height: '100px' }}
                onChange={e => this.handdleChange(e)}
                disabled={this.state.formType === 'ใบลาพักผ่อน'}
              />
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col xs='4'>
            <FormGroup>
              <Label>ช่วงเวลา</Label>
              <Label style={{ float: 'right', marginLeft: '10px' }}>วัน</Label>
              <Input
                type='number'
                placeholder='0'
                min={0}
                step='1'
                style={{
                  float: 'right',
                  width: '60px',
                  bottom: '5px',
                  position: 'relative'
                }}
              ></Input>
              <Label style={{ float: 'right', marginRight: '10px' }}>
                มีกำหนด
              </Label>
              <DateRange
                onChange={e => this.handleSelect(e)}
                moveRangeOnFirstSelection={false}
                ranges={[this.state.selectionRange]}
                showPreview={true}
              />
            </FormGroup>
          </Col>
          <Col xs='8'>
            <FormGroup>
              <Label for='exampleEmail'>ครั้งสุดท้ายตั้งแต่</Label>
              <br />{' '}
              <Input
                type='text'
                name='info'
                id='exampleText'
                disabled
                placeholder='ข้อมูลการลาครั้งล่าสุด'
              />
            </FormGroup>
            <FormGroup>
              <Label for='exampleEmail'>
                ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่
              </Label>
              <Input type='text' placeholder='ข้อมูลการติดต่อระหว่างลา'></Input>
            </FormGroup>
            <FormGroup>
              <Label for='exampleEmail'>
                สถิติการลาในปีงบประมาณนี้ (วันทำการ)
              </Label>
              <Table size='sm'>
                <thead>
                  <tr>
                    <th>ประเภทการลา</th>
                    <th>ลามาแล้ว</th>
                    <th>ลาครั้งนี้</th>
                    <th>รวมเป็น</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>ป่วย</td>
                  </tr>
                  <tr>
                    <td>กิจส่วนตัว</td>
                  </tr>
                  <tr>
                    <td>คลอดบุตร</td>
                  </tr>
                  <tr>
                    <td>ลาพักผ่อน</td>
                  </tr>
                </tbody>
              </Table>
            </FormGroup>
          </Col>
        </Row>
        <Button onClick={() => this.handleSubmit()}>ยืนยัน</Button>
      </Container>
    )
  }
}

export default App
