import axios from 'axios'
import UpdatePage from '../components/Update'
import ConfirmDeletePage from '../components/ConfirmDelete'
import React, { Component } from 'react'
import {
  Button,
  FormGroup,
  Label,
  Input,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Container,
  Table,
  Row,
  Col,
  Badge
} from 'reactstrap'
import '../css/style.css'
import { header } from './string'
import * as IconsBi from 'react-icons/bi'
import * as IconsRi from 'react-icons/ri'

class App extends Component {
  constructor (props) {
    axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN'
    axios.defaults.xsrfCookieName = 'csrftoken'
    super(props)
    if (props.logout === true) {
      this.onLogout()
    }
    const today = new Date()
    this.state = {
      headers: header,
      monthSelected:
        today.getFullYear() +
        '-' +
        (today.getMonth() + 1 < 10 ? '0' : '') +
        (today.getMonth() + 1),
      departmentSelected: '',
      List: [],
      selectionDepartment: [],
      displayList: [],
      modal: false,
      activeItem: {},
      departmentName: [],
      confirm: false
    }
  }

  toggle = () => this.setState({ modal: !this.state.modal })
  toggleConfirm = item =>
    this.setState({ activeItem: item, confirm: !this.state.confirm })

  onLogout = () => {
    localStorage.clear()
    window.location.href = '/'
  }

  monthString = month => {
    const monthNamelist = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ]
    if (typeof month !== undefined) {
      const monthToString =
        month.substring(0, 4) +
        ' ' +
        monthNamelist[parseInt(month.slice(5)) - 1]

      return monthToString
    }
    const monthToString =
      this.state.monthSelected.substring(0, 4) +
      ' ' +
      monthNamelist[parseInt(this.state.monthSelected.slice(5)) - 1]

    return monthToString
  }

  async componentDidMount () {
    this.refreshList()
  }

  refreshList = () => {
    axios
      .get('api/departments/')
      .then(res =>
        this.setState({ selectionDepartment: res.data }, () =>
          this.setState({ selectedDepartment: res.data[0].Off_id })
        )
      )
      .then(() =>
        axios.get(`/api/schedules/`).then(res => {
          this.setState({ List: res.data }, () =>
            this.setState(
              {
                displayList: this.state.List.filter(
                  e =>
                    e.Off_id === this.state.selectedDepartment &&
                    e.month === this.state.monthSelected
                )
              },
              () =>
                this.searchingDepartmentName(this.state.displayList).then(res =>
                  this.setState({ departmentName: res })
                )
            )
          )
        })
      )
  }

  searchingDepartmentName = async data => {
    return await axios.all(
      data.map(item =>
        axios
          .get('api/department/', {
            params: { search: item.Off_id }
          })
          .then(res => res.data.Off_name)
      )
    )
  }

  handleSubmit = item => {
    this.toggle()
    if (item.id) {
      axios
        .put(`/api/schedules/${item.id}/`, item)
        .then(alert('Already edited'))
        .then(() => this.refreshList())
      return
    }
    // axios
    //   .post('api/schedules/', {
    //     name: localStorage.getItem('username'),
    //     month: monthValue,
    //     department: department,
    //     work_schedule: e
    //   })
  }

  searchingList = () => {
    // console.log(new Date(this.state.monthSelected))
    // console.log(this.state.departmentName)

    if (
      this.state.List.filter(
        e =>
          e.Off_id === this.state.selectedDepartment ||
          e.month === this.state.monthSelected
      ).length === 0
    ) {
      this.refreshList()
    } else {
      this.setState(
        {
          displayList: this.state.List.filter(
            e =>
              e.Off_id === this.state.selectedDepartment &&
              e.month === this.state.monthSelected
          )
        },
        () =>
          this.searchingDepartmentName(this.state.displayList).then(res =>
            this.setState({ departmentName: res })
          )
      )
    }
  }

  handleDelete = item => {
    axios
      .delete(`/api/schedules/${item}`)
      .then(res => this.refreshList())
      .then(this.toggleConfirm())
  }

  editItem = item => {
    this.setState({ activeItem: item, modal: !this.state.modal })
  }

  handdleSelect = e => {
    if (e.target.name === 'month') {
      return this.setState({ monthSelected: e.target.value }, () =>
        this.searchingList()
      )
    }
    this.setState({ selectedDepartment: e.target.value }, () =>
      this.searchingList()
    )
  }

  spanSchedule = element => {
    var array = element.toString().split('/')
    var first_span
    var sec_span
    if (array.length !== 1) {
      first_span = this.selectedSpanColor(array[0])
      sec_span = this.selectedSpanColor(array[1])
      return (
        <div>
          {first_span}
          {sec_span}
        </div>
      )
    }
    if (['ช', 'บ', 'ด', 'ล'].includes(element)) {
      return this.selectedSpanColor(element)
    }
    return element
  }

  selectedSpanColor = element => {
    if (element === 'ช') {
      return (
        <Badge pill className='dot' id='morning'>
          <span className='dotText'>ช</span>
        </Badge>
      )
    } else if (element === 'บ') {
      return (
        <Badge pill className='dot' id='afternoon'>
          <span className='dotText'>บ</span>
        </Badge>
      )
    } else if (element === 'ด') {
      return (
        <Badge pill className='dot' id='night'>
          <span className='dotText'>ด</span>
        </Badge>
      )
    } else {
      return (
        <Badge pill className='dot' id='leave'>
          <span className='dotText'>ล</span>
        </Badge>
      )
    }
  }

  renderItem = () => {
    return this.state.displayList.map((item, tindex) => (
      <div key={item.id}>
        <Card>
          <CardBody>
            <CardTitle>
              <Row>
                {/* {this.getDepartmentName(item.Off_id).then(res => (
                  console.log(res)
                ))} */}
                {/* {this.getDepartmentName(item.Off_id).then(res => {
                  return <Col>{res.Off_name}</Col>
                })} */}
                <Col>{this.state.departmentName[tindex]}</Col>
              </Row>
            </CardTitle>
            <CardSubtitle>{this.monthString(item.month)}</CardSubtitle>
            <p style={{ float: 'right', display: 'inline-block' }}>
              <span
                className='dot'
                id='leave'
                style={{ margin: '0px 10px 0px 10px' }}
              ></span>
              <span>ลา</span>
              <span
                className='dot'
                id='morning'
                style={{ margin: '0px 10px 0px 10px' }}
              ></span>
              <span>เช้า</span>
              <span
                className='dot'
                id='afternoon'
                style={{ margin: '0px 10px 0px 10px' }}
              ></span>
              <span>บ่าย</span>
              <span
                className='dot'
                id='night'
                style={{ margin: '0px 10px 0px 10px' }}
              ></span>
              <span>ดึก</span>
            </p>
            <Table striped bordered hover size='sm'>
              <thead>
                <tr className='thead-table'>
                  {this.state.headers.map((header, index) => {
                    return <th key={index}>{header}</th>
                  })}
                </tr>
              </thead>
              <tbody>
                {item.work_schedule.map((item, i) => (
                  <tr key={i} className='td-data'>
                    {Object.values(item).map((element, index) => (
                      <td key={index}>{this.spanSchedule(element)}</td>
                    ))}
                  </tr>
                ))}
              </tbody>
            </Table>

            {localStorage.getItem('group') === 'admin' ||
            localStorage.getItem('username') === item.name ? (
              <div>
                <Button
                  color='warning'
                  onClick={() => this.editItem(item)}
                  className='button'
                  id='edit'
                >
                  <IconsBi.BiEdit id='button-icon' />
                  แก้ไข
                </Button>
                &nbsp;
                <Button
                  color='danger'
                  onClick={() => this.toggleConfirm(item)}
                  className='button'
                  id='remove'
                >
                  <IconsRi.RiDeleteBin5Line id='button-icon' />
                  ลบ
                </Button>
              </div>
            ) : null}
          </CardBody>
        </Card>
        <br></br>
      </div>
    ))
  }

  render () {
    return (
      <Container fluid={true} style={{ position: 'relative' }}>
        <div>
          <FormGroup>
            <Row>
              <Col>
                <Label for='exampleSelect'>แผนก</Label>
                <Input
                  type='select'
                  name='select'
                  id='exampleSelect'
                  onChange={e => this.handdleSelect(e)}
                >
                  {this.state.selectionDepartment.map(item => (
                    <option key={item.Off_id} value={item.Off_id}>
                      {item.Off_name}
                    </option>
                  ))}
                </Input>
              </Col>
            </Row>
            <Label for='exampleSearch'>เดือน</Label>
            <Input
              type='month'
              name='month'
              id='monthSelection'
              value={this.state.monthSelected}
              onChange={e => this.handdleSelect(e)}
            />
          </FormGroup>
          <div>
            <ul className='list-group list-group-flush border-top-0'>
              {this.renderItem()}
            </ul>
          </div>
        </div>
        {this.state.modal ? (
          <UpdatePage
            toggle={this.toggle}
            activeItem={this.state.activeItem}
            onSave={this.handleSubmit}
          />
        ) : null}
        {this.state.confirm ? (
          <ConfirmDeletePage
            toggle={this.toggleConfirm}
            activeItem={this.state.activeItem}
            onDelete={this.handleDelete}
          />
        ) : null}
      </Container>
    )
  }
}
export default App
