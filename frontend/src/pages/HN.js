import { Component } from 'react'
import {
  Button,
  Form,
  FormGroup,
  Row,
  Col,
  CustomInput,
  Label,
  Input,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from 'reactstrap'
import * as XLSX from 'xlsx'
import axios from 'axios'

class App extends Component {
  constructor (props) {
    super(props)
    axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN'
    axios.defaults.xsrfCookieName = 'csrftoken'
    this.state = {
      items: [],
      headers: [],
      List: [],
      displayList: [],
      search: '',
      currentNavigation: 1
    }
  }

  componentDidMount () {
    this.refreshList()
  }

  readExcel = file => {
    const promise = new Promise((resolve, reject) => {
      const fileReader = new FileReader()
      fileReader.readAsArrayBuffer(file)

      fileReader.onload = e => {
        const bufferArray = e.target.result

        const wb = XLSX.read(bufferArray, { type: 'buffer' })

        const wsname = wb.SheetNames[0]

        const ws = wb.Sheets[wsname]

        const data = XLSX.utils.sheet_to_json(ws)

        resolve(data)
      }

      fileReader.onerror = error => {
        reject(error)
      }
    })

    promise.then(d => {
      this.setState({ headers: Object.keys(d[0]) })
      this.setState({ items: d })
    })
  }

  refreshList = () => {
    axios.get('api/hnusers/').then(res =>
      this.setState({
        List: res.data.filter(data => {
          if (
            data.fname.includes(this.state.search) ||
            data.lname.includes(this.state.search) ||
            data.hn.includes(this.state.search)
          ) {
            return data
          }
          return null
        })
      })
    )
  }

  handdleSearch = e => {
    this.setState({ search: e.target.value, currentNavigation: 1 }, () =>
      this.refreshList()
    )
  }

  handdleSubmit = items => {
    items.forEach(data => {
      axios.post('api/hnusers/', data)
      return
    })
  }

  handdleClick = e => {
    if (e.target.name === 'first') {
      return this.setState({ currentNavigation: 1 })
    } else if (e.target.name === 'prev') {
      return this.setState({
        currentNavigation: this.state.currentNavigation - 1
      })
    } else if (e.target.name === 'next') {
      return this.setState({
        currentNavigation: this.state.currentNavigation + 1
      })
    } else if (e.target.name === 'last') {
      return this.setState({
        currentNavigation: Math.ceil(this.state.List.length / 10)
      })
    }
    return this.setState({
      currentNavigation: Number(e.target.name)
    })
  }

  renderPagination = () => {
    let pagination = []
    for (let i = 1; i <= Math.ceil(this.state.List.length / 10); i++) {
      pagination.push(
        <PaginationItem key={i}>
          <PaginationLink name={i} onClick={this.handdleClick}>
            {i}
          </PaginationLink>
        </PaginationItem>
      )
    }
    return pagination
  }

  renderItem = () => {
    return (
      <Table bordered hover>
        <thead>
          <tr>
            <th>HN</th>
            <th>ชื่อ</th>
            <th>นามสกุล</th>
            <th>เบอร์โทรศัพท์</th>
          </tr>
        </thead>
        <tbody>
          {this.state.List.filter(
            (data, index) =>
              Math.ceil((index + 1) / 10) === this.state.currentNavigation
          ).map(item => (
            <tr key={item.id}>
              <td className='table-column'>{item.hn}</td>
              <td className='table-column'>{item.fname}</td>
              <td className='table-column'>{item.lname}</td>
              <td className='table-column'>{item.phone}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    )
  }

  render () {
    return (
      <div>
        <Row>
          {/* <Col>
            <Form>
              <FormGroup>
                <CustomInput
                  type='file'
                  id='exampleCustomFileBrowser'
                  name='file_upload'
                  onChange={e => {
                    const file = e.target.files[0]
                    this.readExcel(file)
                  }}
                />
              </FormGroup>
            </Form>
          </Col>
          {localStorage.getItem('group') === 'admin' ? (
            <Col xs='auto'>
              <Button
                color='success'
                onClick={() => this.handdleSubmit(this.state.items)}
              >
                เพิ่มข้อมูล
              </Button>
            </Col>
          ) : null} */}
        </Row>
        <Label>ค้นหา</Label>
        <Row>
          <Col>
            <Form>
              <FormGroup>
                <Input
                  type='search'
                  name='search'
                  id='exampleSearch'
                  placeholder='กรุณาใส่หมายเลข HN ชื่อ หรือนามสกุล'
                  value={this.state.search}
                  onChange={e => this.handdleSearch(e)}
                />
              </FormGroup>
            </Form>
          </Col>
        </Row>
        <div className='navigation'>
          <Pagination aria-label='Page navigation example'>
            {this.state.currentNavigation > 1 ? (
              <PaginationItem>
                <PaginationLink name='first' onClick={this.handdleClick}>
                  {'<<'}
                </PaginationLink>
              </PaginationItem>
            ) : null}
            {this.state.currentNavigation > 1 ? (
              <PaginationItem>
                <PaginationLink name='prev' onClick={this.handdleClick}>
                  {'<'}
                </PaginationLink>
              </PaginationItem>
            ) : null}

            {Math.ceil(this.state.List.length / 10) === 1
              ? null
              : this.renderPagination()}
            {this.state.currentNavigation <
            Math.ceil(this.state.List.length / 10) ? (
              <PaginationItem>
                <PaginationLink name='next' onClick={this.handdleClick}>
                  {'>'}
                </PaginationLink>
              </PaginationItem>
            ) : null}
            {this.state.currentNavigation <
            Math.ceil(this.state.List.length / 10) ? (
              <PaginationItem>
                <PaginationLink name='last' onClick={this.handdleClick}>
                  {'>>'}
                </PaginationLink>
              </PaginationItem>
            ) : null}
          </Pagination>
        </div>
        <div>{this.renderItem()}</div>
      </div>
    )
  }
}

export default App
