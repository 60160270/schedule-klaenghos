import axios from 'axios'
import React, { Component } from 'react'
import {
  CustomInput,
  Button,
  Table,
  Form,
  FormGroup,
  Label,
  Input,
  Container,
  Badge
} from 'reactstrap'
import * as XLSX from 'xlsx'
import '../css/style.css'
import { header } from './string'

class App extends Component {
  constructor (props) {
    super(props)
    axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN'
    axios.defaults.xsrfCookieName = 'csrftoken'
    const today = new Date()
    this.state = {
      items: [],
      monthSelected:
        today.getFullYear() +
        '-' +
        (today.getMonth() + 1 < 10 ? '0' : '') +
        (today.getMonth() + 1),
      headers: ['กรุณาใส่ข้อมูล'],
      selectionDepartment: [],
      selectedDepartment: [],
      userInfo: []
    }
  }

  componentDidMount () {
    axios
      .get('api/departments/')
      .then(res =>
        this.setState({ selectionDepartment: res.data }, () =>
          this.setState({ selectedDepartment: res.data[0].Off_id })
        )
      )
  }

  monthString = () => {
    const monthNamelist = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ]

    const monthToString =
      this.state.monthSelected.substring(0, 4) +
      ' ' +
      monthNamelist[parseInt(this.state.monthSelected.slice(5)) - 1]

    return monthToString
  }

  readExcel = file => {
    const promise = new Promise((resolve, reject) => {
      const fileReader = new FileReader()
      fileReader.readAsArrayBuffer(file)

      fileReader.onload = e => {
        const bufferArray = e.target.result

        const wb = XLSX.read(bufferArray, { type: 'buffer' })

        const wsname = wb.SheetNames[1]

        const ws = wb.Sheets[wsname]

        const data = XLSX.utils.sheet_to_json(ws)

        resolve(data)
      }

      fileReader.onerror = error => {
        reject(error)
      }
    })

    promise.then(d => {
      this.searchingUserInfo(d).then(res =>
        this.setState({ userInfo: res }, () => this.mergeUserInfo(d))
      )
    })
  }

  searchingUserInfo = async user => {
    return await axios.all(user.map(item => axios.get(`api/hn/${item.hn}`)))
  }

  mergeUserInfo = async table => {
    new Promise((resolve, reject) => {
      this.state.userInfo.forEach((user, index) => {
        var arr = {
          hn: user.data.hn,
          fname: user.data.fname,
          lname: user.data.lname
        }
        table[index].phone = user.data.phone
        Object.assign(arr, table[index])
        table[index] = arr
      })
      resolve(table)
    }).then(res => {
      this.setState(
        {
          headers: header.slice(0, header.length - 3)
        },
        () => this.setState({ items: res })
      )
    })
  }

  handdleSubmit = e => {
    axios
      .post('api/schedules/', {
        name: localStorage.getItem('username'),
        month: this.state.monthSelected,
        Off_id: this.state.selectedDepartment,
        work_schedule: e
      })
      .then(alert('Successful upload'))
  }

  optionalAdd = () => {
    this.state.items.forEach(item => {
      var morning = 0
      var afternoon = 0
      var night = 0
      Object.values(item).forEach((element, index) => {
        if (element.toString().length > 3 || typeof element === 'number') {
        } else if (element.toString().length === 1) {
          if (element === 'ช') {
            morning = morning + 1
          } else if (element === 'บ') {
            afternoon = afternoon + 1
          } else if (element === 'ด') {
            night = night + 1
          }
        } else if (element.toString().length === 3) {
          var array = element.split('/')
          array.forEach(e => {
            if (e === 'ช') {
              morning = morning + 1
            } else if (e === 'บ') {
              afternoon = afternoon + 1
            } else if (e === 'ด') {
              night = night + 1
            }
          })
        }
      })
      var data = {
        morning: morning,
        afternoon: afternoon,
        night: night
      }
      Object.assign(item, data)
    })
    this.handdleSubmit(this.state.items)
    window.location.href = '/'
  }

  handdleSelect = e => {
    console.log(e.target.value)
    if (e.target.name === 'month') {
      return this.setState({ monthSelected: e.target.value })
    }
    this.setState({ selectedDepartment: e.target.value })
  }

  spanSchedule = element => {
    var array = element.toString().split('/')
    var first_span
    var sec_span
    if (array.length !== 1) {
      first_span = this.selectedSpanColor(array[0])
      sec_span = this.selectedSpanColor(array[1])
      return (
        <div>
          {first_span}
          {sec_span}
        </div>
      )
    }
    if (['ช', 'บ', 'ด', 'ล'].includes(element)) {
      return this.selectedSpanColor(element)
    }
    return element
  }

  selectedSpanColor = element => {
    if (element === 'ช') {
      return (
        <Badge pill className='dot' id='morning'>
          <span className='dotText'>ช</span>
        </Badge>
      )
    } else if (element === 'บ') {
      return (
        <Badge pill className='dot' id='afternoon'>
          <span className='dotText'>บ</span>
        </Badge>
      )
    } else if (element === 'ด') {
      return (
        <Badge pill className='dot' id='night'>
          <span className='dotText'>ด</span>
        </Badge>
      )
    } else {
      return (
        <Badge pill className='dot' id='leave'>
          <span className='dotText'>ล</span>
        </Badge>
      )
    }
  }

  render () {
    return (
      <Container className='themed-container' fluid={true}>
        <br></br>
        <Form>
          <Label for='exampleSearch'>เดือน</Label>
          <Input
            type='month'
            name='month'
            id='monthSelection'
            value={this.state.monthSelected}
            onChange={e => this.setState({ monthSelected: e.target.value })}
          />
          <FormGroup>
            <Label for='exampleSelect'>แผนก</Label>
            <Input
              type='select'
              name='select'
              id='exampleSelect'
              onChange={e => this.handdleSelect(e)}
            >
              {this.state.selectionDepartment.map(item => (
                <option key={item.Off_id} value={item.Off_id}>
                  {item.Off_name}
                </option>
              ))}
            </Input>
          </FormGroup>
        </Form>

        <CustomInput
          type='file'
          id='exampleCustomFileBrowser'
          name='file_upload'
          label='กรุณาอัปโหลดไฟล์'
          accept='.xlsx, .xls, .csv'
          onChange={e => {
            const file = e.target.files[0]
            this.readExcel(file)
          }}
        />
        <br />
        <br />
        <p style={{ float: 'right', display: 'inline-block' }}>
          <span
            className='dot'
            id='leave'
            style={{ margin: '0px 10px 0px 10px' }}
          ></span>
          <span>ลา</span>
          <span
            className='dot'
            id='morning'
            style={{ margin: '0px 10px 0px 10px' }}
          ></span>
          <span>เช้า</span>
          <span
            className='dot'
            id='afternoon'
            style={{ margin: '0px 10px 0px 10px' }}
          ></span>
          <span>บ่าย</span>
          <span
            className='dot'
            id='night'
            style={{ margin: '0px 10px 0px 10px' }}
          ></span>
          <span>ดึก</span>
        </p>
        <Table striped bordered hover size='sm'>
          <thead>
            <tr className='thead-table'>
              {this.state.headers.map((header, index) => (
                <th scope='col' key={header}>
                  {header}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {this.state.items.map((item, i) => (
              <tr key={i} className='td-data'>
                {Object.values(item).map((element, index) => (
                  <td key={index}>{this.spanSchedule(element)}</td>
                ))}
              </tr>
            ))}
          </tbody>
          <tbody></tbody>
        </Table>
        <Button color='success' onClick={() => this.optionalAdd()}>
          เพิ่มข้อมูล
        </Button>
      </Container>
    )
  }
}

export default App
