import { Component } from 'react'
import { Container, Button, Badge, Row, Col, Table } from 'reactstrap'
import axios from 'axios'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = { List: [] }
  }

  componentDidMount () {
    axios
      .get('api/leaveforms/')
      .then(res =>
        this.setState({ List: res.data })
      )
  }

  render () {
    return (
      <Container>
        <Row>
          <Col>
            <Button color='primary' outline>
              Pending <Badge color='secondary'>4</Badge>
            </Button>
          </Col>
          <Col>
            <Button color='primary' outline>
              Confirmed <Badge color='secondary'>4</Badge>
            </Button>
          </Col>
        </Row>
        <br />
        <Row>
          <Table striped>
            <thead>
              <tr>
                <th>หมายเลข HN</th>
                <th>เริ่มหยุดวันที่</th>
                <th>ถึงวันที่</th>
                <th>สาเหตุ</th>
                <th>สถานะ</th>
                <th> </th>
              </tr>
            </thead>
            <tbody>
              {this.state.List.map((data, i) => (
                <tr key={i}>
                  {Object.values(data)
                    .slice(1)
                    .map((item, index) => (
                      <td key={index}>{item}</td>
                    ))}
                  <td>
                    <Button color='success' size='sm'>
                      Confirm
                    </Button>
                    <Button color='danger' size='sm'>
                      Cancel
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Row>
      </Container>
    )
  }
}
export default App
