from django.contrib import admin
from .models import Schedule,HNUser,Department,LeaveForm
# Register your models here.


class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('name', 'month', 'Off_id', 'work_schedule')

class HNUserAdmin(admin.ModelAdmin):
    list_display = ('hn', 'title', 'fname','lname','sex','address','village','tambon','amphur','province','phone')

class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('Off_id', 'Off_name')

class LeaveFormAdmin(admin.ModelAdmin):
    list_display = ('Off_id','hn','employee_type','position','affiliation','work_group','start_date','end_date','reason','number_leave','contact','status','form_type')

admin.site.register(Schedule, ScheduleAdmin)
admin.site.register(HNUser, HNUserAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(LeaveForm, LeaveFormAdmin)