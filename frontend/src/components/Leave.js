import { Component } from 'react'
import {
  FormGroup,
  Label,
  Input,
  Container,
  Button,
  Col,
  Row,
  Table
} from 'reactstrap'
import axios from 'axios'
import 'react-date-range/dist/styles.css' // main style file
import 'react-date-range/dist/theme/default.css' // theme css file
import { DateRange } from 'react-date-range'

class App extends Component {
  constructor (props) {
    super(props)
    axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN'
    axios.defaults.xsrfCookieName = 'csrftoken'
    this.state = {
      selectionDepartment: [],
      selectedDepartment: [],
      hn: {},
      selectionRange: {
        startDate: new Date(),
        endDate: new Date(),
        key: 'selection'
      },
      reason: {},
      formType: [],
      test: false
    }
  }

  componentDidMount () {
    axios
      .get('api/departments/')
      .then(res =>
        this.setState({ selectionDepartment: res.data }, () =>
          this.setState({ selectedDepartment: res.data[0].Off_id })
        )
      )
  }

  handleSelect = ranges => {
    this.setState({ selectionRange: ranges.selection })
  }

  dateFormat = date => {
    let day = date.getDate()
    let month = date.getMonth()
    let year = date.getFullYear()
    return month + 1 < 10
      ? `${year}-0${month + 1}-${day}`
      : `${year}-${month + 1}-${day}`
  }

  handleSubmit = () => {
    let payload = {
      Off_id: this.state.selectedDepartment,
      hn: this.state.hn,
      start_date: this.dateFormat(this.state.selectionRange.startDate),
      end_date: this.dateFormat(this.state.selectionRange.endDate),
      reason: this.state.reason
    }

    axios.post('api/leaveforms/', payload)
  }

  handdleChange = e => {
    if (e.target.name === 'select') {
      console.log(e.target.value)
      return this.setState({ selectedDepartment: e.target.value })
    } else if (e.target.name === 'hn') {
      return this.setState({ hn: e.target.value })
    } else if (e.target.name === 'formType') {
      return this.setState({ formType: e.target.value })
    }
    return this.setState({ reason: e.target.value })
  }

  render () {
    return (
      <Container>
        <Row>
          <Col xs='8'>
            <FormGroup>
              <Label for='exampleEmail'>HN</Label>
              <Input
                type='text'
                name='hn'
                id='exampleEmail'
                placeholder='HN'
                onChange={e => this.handdleChange(e)}
              />
            </FormGroup>
          </Col>
          <Col sm='4'>
            <FormGroup>
              <Label for='exampleEmail'>เป็น</Label>
              <Input
                type='select'
                name='position'
                id='exampleSelect'
                // onChange={e => this.handdleChange(e)}
              >
                {/* {this.state.formType.map(item => (
              <option key={item}>{item}</option>
            ))} */}
                <option>ข้าราชการ</option>
                <option>พนักงานราชการ</option>
                <option>ลูกจ้างประจำ</option>
                <option>พนักงานกระทรวงสาธารณสุข</option>
                <option>ลูกจ้างชั่วคราว</option>
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col sm='4'>
            <FormGroup>
              <Label>ตำแหน่ง</Label>
              <Input
                type='text'
                name='text'
                id='exampleText'
                onChange={e => this.handdleChange(e)}
              />
            </FormGroup>
          </Col>
          <Col sm='4'>
            <FormGroup>
              <Label>สังกัดจุด/ฝ่าย</Label>
              <Input
                type='text'
                name='text'
                id='exampleText'
                onChange={e => this.handdleChange(e)}
              />
            </FormGroup>
          </Col>
          <Col sm='4'>
            <FormGroup>
              <Label>กลุ่มงาน</Label>
              <Input
                type='text'
                name='text'
                id='exampleText'
                onChange={e => this.handdleChange(e)}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col sm='2'>
            <FormGroup>
              <Label for='exampleSelect'>ประสงค์ขอลา</Label>
              <FormGroup check>
                <Label check>
                  <Input type='radio' name='radio' />
                  ป่วย
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check>
                  <Input type='radio' name='radio' />
                  กิจส่วนตัว
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check>
                  <Input type='radio' name='radio' />
                  ลาคลอดบุตร
                </Label>
              </FormGroup>
            </FormGroup>
          </Col>
          <Col sm='10'>
            <FormGroup>
              <Label for='exampleText'>เนื่องจาก</Label>
              <Input
                type='textarea'
                name='text'
                id='exampleText'
                style={{ height: '150px' }}
                onChange={e => this.handdleChange(e)}
              />
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col xs='4'>
            <FormGroup>
              <Label for='exampleEmail'>ช่วงเวลา</Label>
              <DateRange
                onChange={e => this.handleSelect(e)}
                moveRangeOnFirstSelection={false}
                ranges={[this.state.selectionRange]}
                showPreview={true}
              />
            </FormGroup>
          </Col>
          <Col xs='8'>
            <FormGroup>
              <Label for='exampleEmail'>ครั้งสุดท้ายตั้งแต่</Label>
              <br /> <Label for='exampleEmail'>ข้อมูล</Label>
            </FormGroup>
            <FormGroup>
              <Label for='exampleEmail'>
                ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่
              </Label>
              <Input type='text' placeholder='ข้อมูลการติดต่อระหว่างลา'></Input>
            </FormGroup>
            <FormGroup>
              <Label for='exampleEmail'>
                สถิติการลาในปีงบประมาณนี้ (วันทำการ)
              </Label>
              <Table size='sm'>
                <thead>
                  <tr>
                    <th>ประเภทการลา</th>
                    <th>ลามาแล้ว</th>
                    <th>ลาครั้งนี้</th>
                    <th>รวมเป็น</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>ป่วย</td>
                  </tr>
                  <tr>
                    <td>กิจส่วนตัว</td>
                  </tr>
                  <tr>
                    <td>คลอดบุตร</td>
                  </tr>
                </tbody>
              </Table>
            </FormGroup>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default App
