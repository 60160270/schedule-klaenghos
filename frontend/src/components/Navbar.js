import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { NavItem, NavLink, Container, Row, Col } from 'reactstrap'
import * as IconsGi from 'react-icons/gi'
import * as IconsAi from 'react-icons/ai'
import * as IconsFa from 'react-icons/fa'
import * as IconsBs from 'react-icons/bs'
import * as IconsIo5 from 'react-icons/io5'
import * as IconsIo from 'react-icons/io'
import AddItemPage from '../pages/AddItem'
import HomePage from '../pages/Home'
import LoginPage from '../pages/Login'
import FormPage from '../pages/Form'
import HNPage from '../pages/HN'
import HomeFormPage from '../pages/HomeForm'
import DepartmentPage from '../components/Department'
import '../css/style.css'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isOpen: false,
      collapsed: true,
      sidebar: false
    }
  }
  showMenubar = () => {
    this.setState({ sidebar: !this.state.sidebar })
    this.state.sidebar
      ? (document.getElementById('overlay').style.display = 'none')
      : (document.getElementById('overlay').style.display = 'block')
  }
  toggle = () => {
    this.setState({ isOpen: !this.state.isOpen })
  }
  Home = () => {
    return <HomePage user={this.state.currentUser} />
  }
  HomeFormPage = () => {
    return <HomeFormPage user={this.state.currentUser} />
  }
  FormPage = () => {
    return <FormPage user={this.state.currentUser} />
  }
  AddPage = () => {
    return <AddItemPage user={this.state.currentUser} />
  }
  Login = () => {
    return <LoginPage user={this.state.currentUser} />
  }
  Logout = () => {
    return <HomePage user={this.state.currentUser} logout={true} />
  }
  HN = () => {
    return <HNPage user={this.state.currentUser} />
  }
  Department = () => {
    return <DepartmentPage user={this.state.currentUser} />
  }

  toggleNavbar = () => this.setState({ collapsed: !this.state.collapsed })

  render () {
    return (
      <Router>
        <Container
          className='themed-container'
          fluid={true}
          style={{
            position: 'absolute',
            backgroundColor: '#F5F5F5',
            width: '100%',
            height: '100%'
          }}
        >
          <Row className='navbar'>
            <div>
              <Col>
                <NavLink href='#'>
                  <IconsGi.GiHamburgerMenu
                    onClick={() => this.showMenubar()}
                    className='menu-bars'
                  />
                </NavLink>
              </Col>
            </div>
            <nav
              className={this.state.sidebar ? 'nav-menu active' : 'nav-menu'}
              style={{ zIndex: '3' }}
            >
              {localStorage.getItem('username') === null ? (
                <ul className='nav-menu-items'>
                  <IconsIo.IoMdCloseCircle
                    className='navbar-toggle'
                    onClick={() => this.showMenubar()}
                  />
                  <br></br>
                  <br></br>
                  <NavItem className='nav-text'>
                    <NavLink href='/'>
                      <IconsFa.FaHome className='icon-menu' />
                      หน้าหลัก
                    </NavLink>
                  </NavItem>
                  <NavItem className='nav-text'>
                    <NavLink href='/hn'>
                      <IconsBs.BsFillPersonLinesFill className='icon-menu' />
                      บุคคล
                    </NavLink>
                  </NavItem>
                  {/* กำลังแก้ไข */}
                  {/* <NavItem className='nav-text'>
                    <NavLink href='/form'>
                      <IconsBs.BsFillPersonLinesFill className='icon-menu' />
                      ลากิจ
                    </NavLink>
                  </NavItem>
                  <NavItem className='nav-text'>
                    <NavLink href='/homeform'>
                      <IconsBs.BsFillPersonLinesFill className='icon-menu' />
                      สถานะฟอร์ม
                    </NavLink>
                  </NavItem> */}
                  <NavItem className='nav-text'>
                    <NavLink href='/login'>
                      <IconsIo5.IoLogIn className='icon-menu' />
                      ล็อคอิน
                    </NavLink>
                  </NavItem>
                </ul>
              ) : (
                <ul className='nav-menu-items'>
                  <IconsIo.IoMdCloseCircle
                    className='navbar-toggle'
                    onClick={() => this.showMenubar()}
                  />
                  <br></br>
                  <br></br>
                  <NavItem className='nav-text'>
                    <NavLink href='/'>
                      <IconsFa.FaHome className='icon-menu' />
                      หน้าหลัก
                    </NavLink>
                  </NavItem>
                  <NavItem className='nav-text'>
                    <NavLink href='/add'>
                      <IconsAi.AiFillSchedule className='icon-menu' />
                      เพิ่มตารางเวร
                    </NavLink>
                  </NavItem>
                  <NavItem className='nav-text'>
                    <NavLink href='/hn'>
                      <IconsBs.BsFillPersonLinesFill className='icon-menu' />
                      บุคคล
                    </NavLink>
                  </NavItem>
                  {/* <NavItem className='nav-text'>
                    <NavLink href='/department'>
                      <IconsAi.AiFillCloud className='icon-menu' />
                      แผนก
                    </NavLink>
                  </NavItem> */}
                  <NavItem className='nav-text'>
                    <NavLink href='/logout'>
                      <IconsIo5.IoLogOut className='icon-menu' />
                      ล็อคเอาท์
                    </NavLink>
                  </NavItem>
                </ul>
              )}
            </nav>
            <br />
            <br />
          </Row>
          <div id='overlay' onClick={() => this.showMenubar()}></div>
          <Row className='Component'>
            <Col xs='12'>
              <div>
                <Route exact path='/' component={this.Home} />
                <Route path='/add' component={this.AddPage} />
                <Route path='/form' component={this.FormPage} />
                <Route path='/homeform' component={this.HomeFormPage} />
                <Route path='/hn' component={this.HN} />
                <Route path='/department' component={this.Department} />
                <Route path='/login' component={this.Login} />
                <Route path='/logout' component={this.Logout} />
              </div>
            </Col>
          </Row>
        </Container>
      </Router>
    )
  }
}

export default App
