import React, { Component } from 'react'
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  CustomInput,
  Button,
  Table,
  Form,
  Input,
  Label,
  FormGroup,
  Container,
  Badge
} from 'reactstrap'
import * as XLSX from 'xlsx'
import axios from 'axios'
import { header } from '../pages/string'

class App extends Component {
  constructor (props) {
    super(props)
    axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN'
    axios.defaults.xsrfCookieName = 'csrftoken'
    this.state = {
      activeItem: props.activeItem,
      items: props.activeItem.work_schedule,
      monthSelected: props.activeItem.month,
      selectionDepartment: [],
      selectedDepartment: props.activeItem.Off_id,
      headers: header,
      toggle: props.toggle,
      onSave: props.onSave
    }
  }

  componentDidMount () {
    axios
      .get('api/departments/')
      .then(res => this.setState({ selectionDepartment: res.data }))
  }

  readExcel = file => {
    const promise = new Promise((resolve, reject) => {
      const fileReader = new FileReader()
      fileReader.readAsArrayBuffer(file)

      fileReader.onload = e => {
        const bufferArray = e.target.result

        const wb = XLSX.read(bufferArray, { type: 'buffer' })

        const wsname = wb.SheetNames[1]

        const ws = wb.Sheets[wsname]

        const data = XLSX.utils.sheet_to_json(ws)

        resolve(data)
      }

      fileReader.onerror = error => {
        reject(error)
      }
    })

    promise.then(d => {
      this.searchingUserInfo(d).then(res =>
        this.setState({ userInfo: res }, () => this.mergeUserInfo(d))
      )
    })
  }

  searchingUserInfo = async user => {
    return await axios.all(user.map(item => axios.get(`api/hn/${item.hn}`)))
  }

  mergeUserInfo = async table => {
    new Promise((resolve, reject) => {
      this.state.userInfo.forEach((user, index) => {
        var arr = {
          hn: user.data.hn,
          fname: user.data.fname,
          lname: user.data.lname
        }
        table[index].phone = user.data.phone
        Object.assign(arr, table[index])
        table[index] = arr
      })
      resolve(table)
    }).then(res =>
      this.setState({ headers: header.slice(0, header.length - 3) }, () =>
        this.setState({ items: res })
      )
    )
  }

  handdleOnSave = item => {
    var setActiveItem = {
      ...this.state.activeItem,
      Off_id: this.state.selectedDepartment,
      month: this.state.monthSelected,
      work_schedule: item
    }
    this.state.onSave(setActiveItem)
  }

  handdleSelect = e => {
    if (e.target.name === 'month') {
      return this.setState({ monthSelected: e.target.value })
    }
    this.setState({ selectedDepartment: e.target.value })
  }

  spanSchedule = element => {
    var array = element.toString().split('/')
    var first_span
    var sec_span
    if (array.length !== 1) {
      first_span = this.selectedSpanColor(array[0])
      sec_span = this.selectedSpanColor(array[1])
      return (
        <div>
          {first_span}
          {sec_span}
        </div>
      )
    }
    if (['ช', 'บ', 'ด'].includes(element)) {
      return this.selectedSpanColor(element)
    }
    return element
  }

  selectedSpanColor = element => {
    if (element === 'ช') {
      return (
        <Badge pill className='dot' id='morning'>
          <span className='dotText'>ช</span>
        </Badge>
      )
    } else if (element === 'บ') {
      return (
        <Badge pill className='dot' id='afternoon'>
          <span className='dotText'>บ</span>
        </Badge>
      )
    } else {
      return (
        <Badge pill className='dot' id='night'>
          <span className='dotText'>ด</span>
        </Badge>
      )
    }
  }

  optionalAdd = () => {
    this.state.items.forEach(item => {
      var morning = 0
      var afternoon = 0
      var evening = 0
      Object.values(item).forEach((element, index) => {
        if (element.toString().length > 3 || typeof element === 'number') {
        } else if (element.toString().length === 1) {
          if (element === 'ช') {
            morning = morning + 1
          } else if (element === 'บ') {
            afternoon = afternoon + 1
          } else if (element === 'ด') {
            evening = evening + 1
          }
        } else if (element.toString().length === 3) {
          var array = element.split('/')
          array.forEach(e => {
            if (e === 'ช') {
              morning = morning + 1
            } else if (e === 'บ') {
              afternoon = afternoon + 1
            } else if (e === 'ด') {
              evening = evening + 1
            }
          })
        }
      })
      var data = {
        morning: morning,
        afternoon: afternoon,
        evening: evening
      }
      Object.assign(item, data)
    })
    this.handdleOnSave(this.state.items)
  }

  render () {
    return (
      <Container className='themed-container' fluid={true}>
        <div>
          <Modal isOpen={true} toggle={this.state.toggle} size='xl' id='modal'>
            <ModalHeader toggle={this.state.toggle}>แก้ไข</ModalHeader>
            <ModalBody>
              <div>
                <br></br>
                <Form>
                  <Label for='exampleSearch'>เดือน</Label>
                  <Input
                    type='month'
                    name='month'
                    id='monthSelection'
                    value={this.state.monthSelected}
                    onChange={e =>
                      this.setState({ monthSelected: e.target.value })
                    }
                  />
                  <FormGroup>
                    <Label for='exampleSelect'>แผนก</Label>
                    <Input
                      type='select'
                      name='select'
                      id='exampleSelect'
                      value={this.state.selectedDepartment}
                      onChange={e => this.handdleSelect(e)}
                    >
                      {this.state.selectionDepartment.map(item => (
                        <option key={item.Off_id} value={item.Off_id}>
                          {item.Off_name}
                        </option>
                      ))}
                    </Input>
                  </FormGroup>
                </Form>

                <CustomInput
                  type='file'
                  id='exampleCustomFileBrowser'
                  name='file_upload'
                  label='กรุณาอัปโหลดไฟล์'
                  onChange={e => {
                    const file = e.target.files[0]
                    this.readExcel(file)
                  }}
                />
                <br />
                <br />
                <p style={{ float: 'right', display: 'inline-block' }}>
                  <span
                    className='dot'
                    id='morning'
                    style={{ margin: '0px 10px 0px 10px' }}
                  ></span>
                  <span>เช้า</span>
                  <span
                    className='dot'
                    id='afternoon'
                    style={{ margin: '0px 10px 0px 10px' }}
                  ></span>
                  <span>บ่าย</span>
                  <span
                    className='dot'
                    id='night'
                    style={{ margin: '0px 10px 0px 10px' }}
                  ></span>
                  <span>ดึก</span>
                </p>
                <Table
                  className='table container'
                  striped
                  bordered
                  hover
                  size='sm'
                  responsive
                  style={{ maxWidth: '100%' }}
                >
                  <thead>
                    <tr className='thead-table'>
                      {this.state.headers.map((header, index) => (
                        <th scope='col' key={header}>
                          {header}
                        </th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.items.map(item => (
                      <tr key={item.hn}>
                        {Object.values(item).map((element, index) => (
                          <td key={index}>{this.spanSchedule(element)}</td>
                        ))}
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button color='success' onClick={() => this.optionalAdd()}>
                บันทึกการแก้ไข
              </Button>
              <Button color='secondary' onClick={() => this.state.toggle()}>
                ยกเลิก
              </Button>
            </ModalFooter>
          </Modal>
        </div>
      </Container>
    )
  }
}
export default App
