import { Component } from 'react'
import {
  Button,
  Form,
  FormGroup,
  Row,
  Col,
  CustomInput,
  Label,
  Input,
} from 'reactstrap'
import * as XLSX from 'xlsx'
import axios from 'axios'

class App extends Component {
  constructor (props) {
    super(props)
    axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN'
    axios.defaults.xsrfCookieName = 'csrftoken'
    this.state = {
      items: [],
      headers: [],
      search: '',
      selectItem: [],
      selectedItem: []
    }
  }

  componentDidMount () {
    axios
      .get('api/departments/')
      .then(res => this.setState({ selectItem: res.data }))
  }

  readExcel = file => {
    const promise = new Promise((resolve, reject) => {
      const fileReader = new FileReader()
      fileReader.readAsArrayBuffer(file)

      fileReader.onload = e => {
        const bufferArray = e.target.result

        const wb = XLSX.read(bufferArray, { type: 'buffer' })

        const wsname = wb.SheetNames[0]

        const ws = wb.Sheets[wsname]

        const data = XLSX.utils.sheet_to_json(ws)

        resolve(data)
      }

      fileReader.onerror = error => {
        reject(error)
      }
    })

    promise.then(d => {
      this.setState({ headers: Object.keys(d[0]) })
      this.setState({ items: d })
    })
  }

  handdleSubmit = items => {
    items.forEach(data => {
      axios.post('api/departments/', data)
      return
    })
  }

  handdleSearch = e => {
    this.setState({ search: e.target.value }, () => {
      axios
        .get('api/department/', { params: { seach: this.state.search } })
        .then(res => console.log(res.data))
    })
  }

  handdleTest = e => {
    this.setState({ selectedItem: e.target.value }, () =>
      console.log(this.state.selectedItem)
    )
  }

  render () {
    return (
      <div>
        <Row>
          <Col>
            <Form>
              <FormGroup>
                <CustomInput
                  type='file'
                  id='exampleCustomFileBrowser'
                  name='file_upload'
                  onChange={e => {
                    const file = e.target.files[0]
                    this.readExcel(file)
                  }}
                />
              </FormGroup>
            </Form>
          </Col>
          <Col xs='auto'>
            <Button
              color='success'
              onClick={() => this.handdleSubmit(this.state.items)}
            >
              เพิ่มข้อมูล
            </Button>
          </Col>
        </Row>
        <Label>ค้นหา</Label>
        <Row>
          <Col>
            <Form>
              <FormGroup>
                <Input
                  type='search'
                  name='search'
                  id='exampleSearch'
                  placeholder='with a placeholder'
                  value={this.state.search}
                  onChange={e => this.handdleSearch(e)}
                />
              </FormGroup>
            </Form>
          </Col>
        </Row>
        <Form>
          <FormGroup>
            <Label for='exampleSelect'>Select</Label>
            <Input
              type='select'
              name='select'
              id='exampleSelect'
              // value={this.state.selectedItem}
              onChange={e => this.handdleTest(e)}
            >
              {this.state.selectItem.map(item => (
                <option key={item.Off_id} value={item.Off_id}>
                  {item.Off_name}
                </option>
              ))}
            </Input>
          </FormGroup>
        </Form>
      </div>
    )
  }
}

export default App
