import { Component } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import axios from 'axios'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      toggle: props.toggle,
      onDelete: props.onDelete,
      activeItem: props.activeItem,
      department: {},
      month: ''
    }
  }

  componentDidMount () {
    this.searchingDepartmentName()
    this.monthString()
  }

  monthString = () => {
    const monthNamelist = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ]
    const monthToString =
      this.state.activeItem.month.substring(0, 4) +
      ' ' +
      monthNamelist[parseInt(this.state.activeItem.month.slice(5)) - 1]

    this.setState({ month: monthToString })
  }

  searchingDepartmentName = () => {
    axios
      .get('api/department/', {
        params: { search: this.state.activeItem.Off_id }
      })
      .then(res => this.setState({ department: res.data }))
  }

  haddleConfirm = () => {
    this.state.onDelete(this.state.activeItem.id)
  }

  render () {
    return (
      <Modal isOpen={true} toggle={this.state.toggle}>
        <ModalHeader toggle={this.state.toggle}>ยืนยันการลบข้อมูล</ModalHeader>
        <ModalBody>
          ท่านต้องการที่จะลบข้อมูล {this.state.department.Off_name}{' '}
          {this.state.month} หรือไม่
        </ModalBody>
        <ModalFooter>
          <Button color='danger' onClick={() => this.haddleConfirm()}>
            ยืนยัน
          </Button>{' '}
          <Button color='secondary' onClick={this.state.toggle}>
            ยกเลิก
          </Button>
        </ModalFooter>
      </Modal>
    )
  }
}

export default App
