from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from .models import Schedule,User,HNUser,Department,LeaveForm


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ('id', 'month', 'name', 'Off_id', 'work_schedule')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'group')

class HNUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = HNUser
        fields = ('id','hn', 'title', 'fname','lname','sex','address','village','tambon','amphur','province','phone')

class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ('Off_id', 'Off_name')

class LeaveFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = LeaveForm
        fields = ('id','Off_id','hn','employee_type','position','affiliation','work_group','start_date','end_date','reason','number_leave','contact','status','form_type')
