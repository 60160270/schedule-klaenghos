from typing import AbstractSet
from django.db import models
from django import forms
# Create your models here.

class User(models.Model):
    username = models.CharField(max_length=120,unique=True)
    group = models.CharField(max_length=100,default="user")

    def _str_(self):
        return self.username

class HNUser(models.Model):
    hn = models.CharField(max_length=120,unique=True)
    title = models.CharField(max_length=20)
    fname = models.CharField(max_length=100)
    lname = models.CharField(max_length=100)
    sex = models.CharField(max_length=3)
    address = models.CharField(max_length=100,default="")
    village = models.CharField(max_length=10,default="")
    tambon = models.CharField(max_length=10,default="")
    amphur = models.CharField(max_length=10,default="")
    province = models.CharField(max_length=10,default="")
    phone = models.CharField(max_length=11,default="")

    def _str_(self):
        return self.hn

class Department(models.Model):
    Off_id = models.CharField(primary_key=True,max_length=3,default="")
    Off_name = models.CharField(max_length=100)

    def _str_(self):
        return self.Off_id

class Schedule(models.Model):
    name = models.CharField(max_length=120)
    month = models.CharField(max_length=100,default="")
    Off_id = models.ForeignKey(Department,on_delete=models.CASCADE,default="")
    work_schedule = models.JSONField(default=dict)

    def _str_(self):
        return self.name

class LeaveForm(models.Model):    
    Off_id = models.ForeignKey(Department,on_delete=models.CASCADE,default="")
    hn = models.CharField(max_length=120)
    employee_type = models.CharField(max_length=50,default="")
    position = models.CharField(max_length=80,default="")
    affiliation = models.CharField(max_length=80,default="")
    work_group = models.CharField(max_length=80,default="")
    start_date = models.DateField()
    end_date = models.DateField()
    reason = models.TextField()   
    number_leave = models.IntegerField(default=0) 
    contact = models.TextField(default="")
    status = models.CharField(max_length=50,default='pending') #  ('pending'),('confirmed'),('canceled')
    form_type = models.CharField(max_length=80,default="")

    def _str_(self):
        return self.hn        